/* request.c: HTTP Request Functions */

#include "spidey.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

int parse_request_method(Request *r);
int parse_request_headers(Request *r);

/**
 * Accept request from server socket.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Newly allocated Request structure.
 *
 * This function does the following:
 *
 *  1. Allocates a request struct initialized to 0.
 *  2. Initializes the headers list in the request struct.
 *  3. Accepts a client connection from the server socket.
 *  4. Looks up the client information and stores it in the request struct.
 *  5. Opens the client socket stream for the request struct.
 *  6. Returns the request struct.
 *
 * The returned request struct must be deallocated using free_request.
 **/
Request * accept_request(int sfd) {
    Request *request;
    struct sockaddr request_addr;
    socklen_t request_len = sizeof(struct sockaddr);

    /* Allocate request struct (zeroed) */
    request = calloc(1, sizeof(Request));
    if(!request){
        debug("Unable to allocate request: %s", strerror(errno));
        goto fail;
    }

    /* Accept a client */
    request->fd = accept(sfd, &request_addr, &request_len);
    if(request->fd < 0) {
        debug("Unable to accept: %s", strerror(errno));
        goto fail;
    }

    /* Lookup client information */
    int status = getnameinfo(&request_addr, request_len, request->host, sizeof(request->host), request->port, sizeof(request->port), NI_NUMERICHOST | NI_NUMERICSERV);
    if(status != 0) {
        debug("Unable to getnameinfo: %s", gai_strerror(status));
        goto fail;
    }

    /* Open socket stream */
    request->stream = fdopen(request->fd, "r+");
    if(!request->stream){
        debug("Unable to fdopen: %s", strerror(errno));
        goto fail;
    }
    log("Accepted request from %s/%s", request->host, request->port);
    return request;

fail:
    /*Deallocate request struct */
    free_request(request);
    return NULL;
}

/**
 * Deallocate request struct.
 *
 * @param   r           Request structure.
 *
 * This function does the following:
 *
 *  1. Closes the request socket stream or file descriptor.
 *  2. Frees all allocated strings in request struct.
 *  3. Frees all of the headers (including any allocated fields).
 *  4. Frees request struct.
 **/
void free_request(Request *r) {
    if (!r) {
        debug("Unable to allocate request: %s", strerror(errno));
        free(r);
    }

    /* Close socket or fd */
    if (r->stream != NULL){
        fclose(r->stream);
    }
    else{
        close(r->fd);
    }

    /* Free allocated strings */
    free(r->query);
    free(r->uri);
    free(r->path);
    free(r->method);

    /* Free headers */
    Header *h = r->headers;
    while(h != NULL) {
        Header *temp = h->next;
        free(h->data);
        free(h->name);
        free(h);
        h = temp;
    }

    /* Free request */
    free(r);
}

/**
 * Parse HTTP Request.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * This function first parses the request method, any query, and then the
 * headers, returning 0 on success, and -1 on error.
 **/
int parse_request(Request *r) {
    /* Parse HTTP Request Method */
    if (parse_request_method(r) != 0) {
        log("parse_request_method failed");
        return -1;
    }

    /* Parse HTTP Requet Headers*/
    if (parse_request_headers(r) != 0 ) {
        log("parse_request_headers failed");
        return -1;
    }
    return 0;
}

/**
 * Parse HTTP Request Method and URI.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Requests come in the form
 *
 *  <METHOD> <URI>[QUERY] HTTP/<VERSION>
 *
 * Examples:
 *
 *  GET / HTTP/1.1
 *  GET /cgi.script?q=foo HTTP/1.0
 *
 * This function extracts the method, uri, and query (if it exists).
 **/
int parse_request_method(Request *r) {
    char buffer[BUFSIZ];
    char *method;
    char *uri;
    char *query;

    /* Read line from socket */
    if (fgets(buffer, BUFSIZ, r->stream)==NULL){
        debug("fgets error: %s", strerror(errno));
        goto fail;
    }

    /* Parse method and uri */
    method = strtok(skip_whitespace(buffer),WHITESPACE);
    if(!method){
        log("strtok failed");
        goto fail;
    }
   
    uri = strtok(NULL, WHITESPACE);
    if (!uri) {
        log("strtok failed");
        goto fail;
    }

    /* Parse query from uri */
    query = strchr(uri, '?');
    if (!query) {
        query = "";
    } 
    else {
        *query++ = '\0';
    }
    
    /* Record method, uri, and query in request struct */
    r->method = strdup(method);
    r->uri = strdup(uri);
    r->query = strdup(query);

    debug("HTTP METHOD: %s", r->method);
    debug("HTTP URI:    %s", r->uri);
    debug("HTTP QUERY:  %s", r->query);

    return 0;

fail:
    return -1;
}

/**
 * Parse HTTP Request Headers.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Headers come in the form:
 *
 *  <NAME>: <DATA>
 *
 * Example:
 *
 *  Host: localhost:8888
 *  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0
 *  Accept: text/html,application/xhtml+xml
 *  Accept-Language: en-US,en;q=0.5
 *  Accept-Encoding: gzip, deflate
 *  Connection: keep-alive
 *
 * This function parses the stream from the request socket using the following
 * pseudo-code:
 *
 *  while (buffer = read_from_socket() and buffer is not empty):
 *      name, data  = buffer.split(':')
 *      header      = new Header(name, data)
 *      headers.append(header)
 **/
int parse_request_headers(Request *r) {
    Header *curr = NULL;
    char buffer[BUFSIZ];
    char *name;
    char *data;

    /* Parse headers from socket */
    while(fgets(buffer,BUFSIZ,r->stream) && strlen(skip_whitespace(buffer))) {
    
        chomp(buffer);
        name = skip_whitespace(buffer);
        data = strchr(skip_whitespace(buffer),':');
        if(!data){
            log("strchr failed");
            goto fail;
        }
        *data++ = '\0';
        if(!name){
            log("skip_whitespace failed");
            goto fail;
        }
   
        Header *header = calloc(1,sizeof(Header));
        header->name = strdup(name);
        header->data = strdup(data);
        header->next = r->headers;
        r->headers = header;
    }
    
    for (curr = r->headers; curr; curr = curr->next) {
        debug("HTTP HEADER %s = %s", curr->name, curr->data);
    }
    return 0;

fail:
    return -1;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
