/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

/* Internal Declarations */
Status handle_browse_request(Request *request);
Status handle_file_request(Request *request);
Status handle_cgi_request(Request *request);
Status handle_error(Request *request, Status status);

/**
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
Status  handle_request(Request *r) {
    Status result = 0;
   // char buffer[BUFSIZ];

    /* Parse request */
    int parse = parse_request(r);
    if (parse != 0) {
        return handle_error(r, HTTP_STATUS_BAD_REQUEST);
    }

    r->path = determine_request_path(r->uri);
    if (!r->path) {
        debug("determine_request_path error: %s\n", strerror(errno));
        return handle_error(r, HTTP_STATUS_BAD_REQUEST);
    }

    struct stat r_stat;
    if (stat(r->path, &r_stat) != 0) {
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    int can_read = access(r->path, R_OK);
    int can_ex = access(r->path, X_OK);

    if (S_ISDIR(r_stat.st_mode)) {
        handle_browse_request(r);
    } else if ((can_read == 0) && (can_ex == 0)) {
        handle_cgi_request(r);
    } else if (can_read == 0) {
        handle_file_request(r);
    } else {
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    /* Determine request path */
    debug("HTTP REQUEST PATH: %s", r->path);

    /* Dispatch to appropriate request handler type based on file type */
    log("HTTP REQUEST STATUS: %s", http_status_string(result));

    return result;
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_browse_request(Request *r) {
    struct dirent **entries;

    /* Open a directory for reading or scanning */
    int n = scandir(r->path, &entries, 0, alphasort);
    if (n < 0) {
        debug("scandir fails: %s", strerror(errno));
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->stream, "HTTP/1.0 200 OK\r\n");
    fprintf(r->stream, "Content-Type: text/html\r\n");
    fprintf(r->stream, "\r\n");

    /* For each entry in directory, emit HTML list item */
    char buffer[BUFSIZ];
    fprintf(r->stream, "<ul>\n");
    for (int i = 0; i < n; i++) {
        if (streq(entries[i]->d_name, ".")) {
            free(entries[i]);
            continue;
        }
        if (streq(r->uri, "/"))
            sprintf(buffer, "%s%s", r->uri, entries[i]->d_name);
        else
            sprintf(buffer, "%s/%s", r->uri, entries[i]->d_name);
        fprintf(r->stream, "<li><a href=\"%s\">%s</a></li>\n", buffer, entries[i]->d_name);
        free(entries[i]);
    }
    fprintf(r->stream, "</ul>\n");

    /* Return OK */
    free(entries);
    return HTTP_STATUS_OK;
}

/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_file_request(Request *r) {
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
    fs = fopen(r->path, "r");
    if (!fs) {
        debug("File open fail: %s", strerror(errno));
        return handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
    }

    /* Determine mimetype */
    mimetype = determine_mimetype(r->path);

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->stream, "HTTP/1.0 200 OK\r\n");
    fprintf(r->stream, "Content-Type: %s\r\n", mimetype); // figure out content-type
    fprintf(r->stream, "\r\n");

    /* Read from file and write to socket in chunks */
    nread = fread(buffer, 1, BUFSIZ, fs);
    while (nread > 0) {
        fwrite(buffer, 1, nread, r->stream);
        nread = fread(buffer, 1, BUFSIZ, fs);
    }

    /* Close file, deallocate mimetype, return OK */
    free(mimetype);
    fclose(fs);
    return HTTP_STATUS_OK;
}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
Status  handle_cgi_request(Request *r) {
    FILE *pfs;

    /* Export CGI environment variables from request:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */
    setenv("QUERY_STRING", r->query, 1);
    setenv("DOCUMENT_ROOT", RootPath, 1);
    setenv("REMOTE_ADDR", r->host, 1);
    setenv("REMOTE_PORT", r->port, 1);
    setenv("REQUEST_METHOD", r->method, 1);
    setenv("REQUEST_URI", r->uri, 1);
    setenv("SCRIPT_FILENAME", r->path, 1);
    setenv("SERVER_PORT", Port, 1);
    
    /* Export CGI environment variables from request headers */
    Header *curr = NULL;
    for (curr = r->headers; curr; curr = curr->next) {
        if (streq(curr->name,"Host"))
            setenv("HTTP_HOST", curr->data, 1);
        else if (streq(curr->name, "User-Agent"))
            setenv("HTTP_USER_AGENT", curr->data, 1);
        else if (streq(curr->name, "Accept"))
            setenv("HTTP_ACCEPT", curr->data, 1);
        else if (streq(curr->name, "Accept-Language"))
            setenv("HTTP_ACCEPT_LANGUAGE", curr->data, 1);
        else if (streq(curr->name, "Accept-Encoding"))
            setenv("HTTP_ACCEPT_ENCODING", curr->data, 1);
        else if (streq(curr->name, "Connection"))
            setenv("HTTP_CONNECTION", curr->data, 1);
    }
    

    /* POpen CGI Script */
    pfs = popen(r->path, "r");
    if (!pfs) {
        debug("popen fail: %s", strerror(errno));
        return handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
    }

    /* Copy data from popen to socket */
    char buffer[BUFSIZ];
    size_t nread = fread(buffer, 1, BUFSIZ, pfs);
    while (nread > 0) {
        fwrite(buffer, 1, nread, r->stream);
        nread = fread(buffer, 1, BUFSIZ, pfs);
    }

    /* Close popen, return OK */
    pclose(pfs);
    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
Status  handle_error(Request *r, Status status) {
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */
    fprintf(r->stream, "HTTP/1.0 %s\r\n", status_string);
    fprintf(r->stream, "Content-Type: text/html\r\n");
    fprintf(r->stream, "\r\n");

    /* Write HTML Description of Error*/
    fprintf(r->stream, "<head>\n");
    fprintf(r->stream, "<h1>%s</h1>\n", status_string);
    fprintf(r->stream, "</head>\r\n");

    FILE *error_stream = fopen("error.html", "r");
    if (!error_stream) {
        debug("error html open fail");
        fprintf(r->stream, "<body>lol... double error</body>\r\n");
        return status;
    }

    char buffer[BUFSIZ];
    size_t nread = fread(buffer, 1, BUFSIZ, error_stream);
    while (nread > 0) {
        fwrite(buffer, 1, nread, r->stream);
        nread = fread(buffer, 1, BUFSIZ, error_stream);
    }
    fclose(error_stream);

    /* Return specified status */
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
