# Project - README

This is the [Final Project] for [CSE 20289 Systems Programming (Spring 2020)].

## Members

- Crystal Colon (ccolon2@nd.edu)
- Annie Hermann (aherman3@nd.edu)

## Demonstration

- [Link to Demonstration Video] (https://drive.google.com/file/d/1VLJlsephdxgtsOJeo7lWZ0Qm5Ft0Jk0S/view?usp=sharing)
- [Link to Presentation](https://docs.google.com/presentation/d/1ocuj8qzMN2BqQh9TyUp3FQmAfAFuYLkv61JVaxRHfsg/edit?usp=sharing)
## Bonus Meme
https://docs.google.com/presentation/d/16T4Axxmg7yUlYkE-vyl8u6azX6h4srQuO2uUqQgowpU/edit?usp=sharing

## Errata

no errors! (ngl we kinda killed it)

## Contributions

Thor - Annie
Makefile - Annie
Requests - Crystal
Forking - Crystal
Single - Bui
Handler - Annie
Utils - Annie & Crystal (50/50)
Spidey.c - Annie
Experiments - Crystal
Shell Scripts - Crystal
Presentation - Crystal
VPS Guru - Annie

[Final Project]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/project.html
[CSE 20289 Systems Programming (Spring 2020)]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/
