CC=		gcc
CFLAGS=		-g -Wall -std=gnu99 -Iinclude -Werror
LD=		gcc
LDFLAGS=	-Llib
AR=		ar
ARFLAGS=	rcs
TARGETS=	bin/spidey \
			lib/libspidey.a

all:		$(TARGETS)


## spidey ##

bin/spidey: src/spidey.o lib/libspidey.a
	$(LD) $(LDFLAGS) -o $@ $^


## libspidey.a ##

lib/libspidey.a: src/forking.o src/handler.o src/request.o src/single.o src/socket.o src/utils.o src/spidey.o
	$(AR) $(ARFLAGS) $@ $^


## src files ##

src/%.o: src/%.c
	$(CC) $(CFLAGS) -c -o $@ $^


test:
	@$(MAKE) -sk test-all

test-all: test-thor test-spidey

test-thor:	bin/thor.py
	@bin/test_thor.sh

test-spidey: bin/spidey
	@bin/test_spidey.sh

clean:
	@echo Cleaning...
	@rm -f $(TARGETS) lib/*.a src/*.o *.log *.input

.PHONY:		all test clean

# TODO: Add rules for bin/spidey, lib/libspidey.a, and any intermediate objects
