#!/usr/bin/env python3

import concurrent.futures
import os
import requests
import sys
import time

# Functions

def usage(status=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-h HAMMERS -t THROWS] URL
    -h  HAMMERS     Number of hammers to utilize (1)
    -t  THROWS      Number of throws per hammer  (1)
    -v              Display verbose output
    ''')
    sys.exit(status)

def hammer(url, throws, verbose, hid):
    ''' Hammer specified url by making multiple throws (ie. HTTP requests).

    - url:      URL to request
    - throws:   How many times to make the request
    - verbose:  Whether or not to display the text of the response
    - hid:      Unique hammer identifier

    Return the average elapsed time of all the throws.
    '''
    totalSecs = 0
    for i in range(throws):
        timeBegin = time.time()
        r = requests.get(url)
        timeEnd = time.time()
        secs = timeEnd - timeBegin
        totalSecs += secs
        print("Hammer: ", hid, ", Throw: ", i, f', Elapsed Time: %.2f'%secs)

        if verbose:
            print(r.text)

    average = totalSecs/throws
    print("Hammer: ", hid, ", AVERAGE", f', Elapsed Time: %.2f'%average)
    return average

def do_hammer(args):
    ''' Use args tuple to call `hammer` '''
    return hammer(*args)

def main():
    hammers = 1
    throws  = 1
    verbose = False
    url = ''
    hid = ''
    args = sys.argv[1:]

    # Parse command line arguments
    while args:
        arg  = args.pop(0)
        if arg.startswith('-'):
            if arg == '-h':
                hammers = int(args.pop(0))
            elif arg == '-t':
                throws = int(args.pop(0))
            elif arg == '-v':
                verbose = True
            else:
                usage(1)
        else:
            url = arg

    if url == '':
        usage(1)

    total_av = 0
    # Create pool of workers and perform throws
    with concurrent.futures.ProcessPoolExecutor(hammers) as executor:
        arguments = ((url, throws, verbose, i) for i in range(hammers))
        indie_av = executor.map(do_hammer, arguments)
        for av in indie_av:
            total_av += av
        print(f'TOTAL AVERAGE ELAPSED TIME: %.2f'%total_av)
        return 0

# Main execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
