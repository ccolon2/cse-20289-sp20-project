#!/bin/bash
echo "Single Latency Tests"
echo "Directory Listings"
./thor.py -t 11 -h 5 http://student10.cse.nd.edu:9079 >> singledir.txt
echo "done"
echo "Static Html File"
./thor.py -t 11 -h 5 http://student10.cse.nd.edu:9079/html/index.html >> singlehtml.txt
echo "done"
echo "CGI scripts"
./thor.py -t 11 -h 5 http://student10.cse.nd.edu:9079/scripts/cowsay.sh?message=hola+crystal&template=default >> singleCGI.txt
echo "Single Latency tests complete"
