echo "Forking Latency Tests"
echo "Directory listings"
./thor.py -t 11 -h 5 http://student10.cse.nd.edu:9079 >> forkingdirectory.txt
echo "Done"
echo "Html"
./thor.py -t 11 -h 5 http://student10.cse.nd.edu:9079/html/index.html >> forkinghtml.txt
echo "Done"
echo "CGI script"
./thor.py -t 11 -h 5 http://student10.cse.nd.edu:9079/scripts/cowsay.sh?message=hola+crystal&template=default >> forkingCGI.txt
echo "Forking Latency Tests Complete"



